# Andrew's Blocklists

> **tl;dr** - **Chibi** for basic blocking, **Kouhai** for balanced blocking, **Senpai** if you want a little more oomph

**My blocklists**:

* **Block** ads, analytics/trackers, malware, ransomware, phishing, malvertising, mobile ads/tracking, fake news (I hate fake news), the Luminati/Hola network, cryptominers, scam retailers, CNAME disguised trackers, and Windows telemetry
* **Reduce** page load times and data/CPU usage
* **Increase** security, privacy, and battery life on portable devices
* Are **lean** thanks to a daily check and removal of any dead/invalid domains
* Are an **aggregate of well-curated blocklists** with minimal false positives
* Are **updated daily** at ~2:00am Eastern Time
* Are **available in different flavours**:
  * **Non-Strict** with Rakuten (*formerly Ebates*) and RedFlagDeals affiliate domains allowed for a great shopping experience (dat cashback dough)
  * **Strict** with Rakuten/RedFlagDeals affiliate domains blocked
* Include a **compressed format** optimized for use with OpenWRT [adblock](https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md) or [simple-adblock](https://github.com/openwrt/packages/blob/master/net/simple-adblock/files/README.md)

Issues: **https://gitlab.com/andryou/block/issues** | License: MIT (https://mit-license.org/)

## Lists

> Last updated **2022-08-24 02:00:01** with [**49461** dead/invalid domains](https://gitlab.com/andryou/block/raw/master/dead) removed

| Name | Description | Download | # | Download | # |
|:---------:|-----------|:-----------:|:-----------:|:-----------:|:--------:|
Chibi | Basic protection. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi) (1.7M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-domains) (1.3M) | 48590 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-compressed) (1.5M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-compressed-domains) (1.2M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/chibi-unbound) (2.4M) | 41928 domains |
Chibi (**Strict**) | ^ with Rakuten and RedFlagDeals affiliate links **blocked**. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-strict) (1.7M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-domains) (1.3M) | 48623 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-compressed) (1.5M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/chibi-strict-compressed-domains) (1.2M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/chibi-strict-unbound) (2.4M) | 41924 domains |
Kouhai | Balanced protection. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai) (2.9M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-domains) (2.2M) | 92201 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-compressed) (2.1M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-compressed-domains) (1.6M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/kouhai-unbound) (3.5M) | 66809 domains |
Kouhai (**Strict**) | ^ with Rakuten and RedFlagDeals affiliate links **blocked**. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict) (2.9M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-domains) (2.2M) | 92240 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-compressed) (2.1M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-compressed-domains) (1.6M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/kouhai-strict-unbound) (3.5M) | 66804 domains |
Senpai | Robust protection. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/senpai) (16M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/senpai-domains) (12M) | 495783 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/senpai-compressed) (7.0M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/senpai-compressed-domains) (5.4M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/senpai-unbound) (12M) | 210532 domains |
Senpai (**Strict**) | ^ with Rakuten and RedFlagDeals affiliate links **blocked**. | [**HOSTS**](https://gitlab.com/andryou/block/raw/master/senpai-strict) (16M) [**DOMAINS**](https://gitlab.com/andryou/block/raw/master/senpai-strict-domains) (12M) | 495900 domains | [**COMPRESSED HOSTS**](https://gitlab.com/andryou/block/raw/master/senpai-strict-compressed) (6.9M) [**COMPRESSED DOMAINS**](https://gitlab.com/andryou/block/raw/master/senpai-strict-compressed-domains) (5.3M) [**UNBOUND**](https://gitlab.com/andryou/block/raw/master/senpai-strict-unbound) (12M) | 208654 domains |

## Usage

* **HOSTS Format** - Blokada, DNS66, AdAway, Hostsman, dnsmasq, Diversion
* **DOMAINS Format** - Pi-Hole, AdGuard, Diversion
* **COMPRESSED HOSTS Format** - OpenWRT [adblock v2.4.0 or newer](https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md) (see [Configuring adblock](#configuring-adblock) below) or [simple-adblock v1.6.3 or newer](https://docs.openwrt.melmac.net/simple-adblock/) (under `Blacklisted Hosts URLs`)
  * If using **adblock** >= v3.8.0, ensure "DNS Blocking Variant" (`adb_dnsvariant` in the config file) is set to `nxdomain` (default: `nxdomain`)
  * If using **Simple AdBlock** >= v1.8.0, ensure "DNS Service" under "Advanced Configuration" (`dns` in the config file) is set to `dnsmasq.conf` ("DNSMASQ Config") or `dnsmasq.servers` ("DNSMASQ Servers File"), or `unbound.adb_list` ("Unbound AdBlock List") if using unbound (default: `dnsmasq.servers`)
* **COMPRESSED DOMAINS Format** - compatible with **Simple AdBlock** under `Blacklisted Domain URLs` (*not Blacklisted Hosts URLs*)
* **UNBOUND Format** - compatible with **Unbound** instances by adding `include: "/path/to/file/kouhai-unbound"` (for example) to `/etc/unbound/unbound.conf` and then running `unbound-control reload`. This command must be run after every list update.

### Configuring adblock

The config file is located at `/etc/config/adblock`.

The below example configuration lines assume your adblock is configured to use TLD compression (adblock v2.4.0+ and if >= v3.8.0, "DNS Blocking Variant" (`adb_dnsvariant`) is set to `nxdomain`). If not, remove `-compressed` from the URL strings below.

If you would like to use the **Strict** version of a list above, use the appropriate link for **DOMAINS** (if TLD compression is unavailable) or **COMPRESSED DOMAINS** (if TLD compression is available) as the `adb_src` value.

```
config source 'andryou_chibi'
	option adb_src 'https://gitlab.com/andryou/block/raw/master/chibi-compressed-domains'
	option adb_src_rset '/^([[:alnum:]_-]+\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower(\$1)}'
	option adb_src_desc 'unified blocklist, daily updates, approx. 41928 entries'
	option enabled '0'

config source 'andryou_kouhai'
	option adb_src 'https://gitlab.com/andryou/block/raw/master/kouhai-compressed-domains'
	option adb_src_rset '/^([[:alnum:]_-]+\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower(\$1)}'
	option adb_src_desc 'unified blocklist, daily updates, approx. 66809 entries'
	option enabled '1'

config source 'andryou_senpai'
	option adb_src 'https://gitlab.com/andryou/block/raw/master/senpai-compressed-domains'
	option adb_src_rset '/^([[:alnum:]_-]+\.)+[[:alpha:]]+([[:space:]]|$)/{print tolower(\$1)}'
	option adb_src_desc 'unified blocklist, daily updates, approx. 210532 entries'
	option enabled '0'
```

**Important**: only enable one of the three lists. As you can see below, there is overlap.

### Sources

| List / Site | Description | License | Chibi | Kouhai | Senpai |
|---------|-----------|:-----------:|:-----------:|:-----------:|:-----------:|
[Andrew's Blocklist](https://gitlab.com/andryou/andrews-settings/raw/master/andrewblocklist) ([site](https://gitlab.com/andryou/andrews-settings)) | My personal blocklist with any nasty domains I come across. | - | ✔ | ✔ | ✔ |
[Fake News](https://raw.githubusercontent.com/marktron/fakenews/master/fakenews) ([site](https://github.com/marktron/fakenews/)) | An in-progress collection of fake news outlets. | MIT | ✔ | ✔ | ✔ |
[lightswitch05 Hate & Junk](https://www.github.developerdan.com/hosts/lists/hate-and-junk-extended.txt) ([site](https://github.com/lightswitch05/hosts)) | This is an opinionated list to block things that I consider to be hateful or just plain junk. | Apache-2.0 | ✔ | ✔ | ✔ |
[Luminati/Hola Block](https://raw.githubusercontent.com/durablenapkin/block/master/luminati.txt) ([site](https://github.com/durablenapkin/block)) | Block the Luminati/Hola network. | MIT | ✔ | ✔ | ✔ |
[Streaming Ads Block](https://raw.githubusercontent.com/durablenapkin/block/master/streaming.txt) ([site](https://github.com/durablenapkin/block)) | Block streaming ads/pop-ups/pop-unders. | MIT | ✔ | ✔ | ✔ |
[NoCoin](https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt) ([site](https://github.com/hoshsadiq/adblock-nocoin-list)) | This is an adblock list to block "browser-based crypto mining". | MIT | ✔ | ✔ | ✔ |
[WindowsSpyBlocker](https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt) ([site](https://github.com/crazy-max/WindowsSpyBlocker)) | Block spying and tracking on Windows systems. | MIT | ✔ | ✔ | ✔ |
[Scam Blocklist](https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/hosts.txt) ([site](https://github.com/durablenapkin/scamblocklist)) | A blocklist to protect users against untrustworthy retail sites. | MIT | ✔ | ✔ | ✔ |
[MVPS hosts file](https://winhelp2002.mvps.org/hosts.txt) ([site](https://winhelp2002.mvps.org/hosts.htm)) | The purpose of this site is to provide the user with a high quality custom HOSTS file. | CC BY-NC-SA 4.0 | ✔ | ✔ | ✔ |
[yoyo.org](https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&showintro=0&mimetype=plaintext&useip=0.0.0.0) ([site](https://pgl.yoyo.org/adservers/)) | Blocking with ad server and tracking server hostnames. | - | ✔ | ✔ | ✔ |
[Dan Pollock – someonewhocares](https://someonewhocares.org/hosts/zero/hosts) ([site](https://someonewhocares.org/hosts/)) | How to make the internet not suck (as much). | non-commercial with attribution | ✔ | ✔ | ✔ |
[URLHaus](https://urlhaus.abuse.ch/downloads/hostfile/) ([site](https://urlhaus.abuse.ch/)) | A project from abuse.ch with the goal of sharing malicious URLs that are being used for malware distribution. | CC0 | ✔ | ✔ | ✔ |
[osint.digitalside.it](https://raw.githubusercontent.com/davidonzo/Threat-Intel/master/lists/latestdomains.piHole.txt) ([site](https://github.com/davidonzo/Threat-Intel)) | DigitalSide Threat-Intel malware domains list. | MIT | ✔ | ✔ | ✔ |
[AdGuard CNAME Disguised Trackers](https://raw.githubusercontent.com/AdguardTeam/cname-trackers/master/combined_disguised_trackers_justdomains.txt) ([site](https://github.com/AdguardTeam/cname-trackers)) | The list of trackers that disguise the real trackers by using CNAME records. | MIT | ✔ | ✔ | ✔ |
[StevenBlack Unified Hosts](https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts) ([site](https://github.com/StevenBlack/hosts)) | This hosts file is a merged collection of hosts from reputable sources, with a dash of crowd sourcing via Github. | MIT |  | ✔ | ✔ |
[lightswitch05 Ads & Tracking](https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt) ([site](https://github.com/lightswitch05/hosts)) | A programmatically expanded list of hosts found to not be on other lists. | Apache-2.0 |  |  | ✔ |
